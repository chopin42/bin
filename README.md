# Bin: Binary installer

**Bin** is a tool that helps you to install binaries. 

## The problem and the solution

The problem is the complexity of installing a binary like a AppImage for example. So instead of doing:

```shell
wget <binary url>
wget <icon url>
mv <iconname> /opt/somefolder/icon.png
mv <filename> /usr/bin/<name of the app>
nano /usr/share/applications/<name of the app>.desktop
```

Then write:

```shell
[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
Terminal=false
Exec=<name of the app>
Name=<name of the app>
Icon=/opt/somefolder/icon
```

You can have the same result in one command:

```shell
bin install <binary url> <name of the app> <icon url>
```

Then to remove it, instead of running:

```shell
sudo su
rm /usr/bin/<name of the app>
rm /usr/share/applications/<name of the app>.desktop
```

You just have to run

```shell
bin remove <name of the app>
```

## How to install Bin

### From source

Paste the following content into your terminal:

```shell
sudo apt install wget python3 python3-pip
pip3 install wget
wget <path>
sudo mv bin /usr/bin/bin
sudo chmod +x /usr/bin/bin
```

### From binary (not available)
> The binary is not available yet.

Paste the following content into your terminal:

```shell
sudo apt install wget
wget https://gitea.com/chopin42/bin/raw/branch/master/binary/bin
sudo mv bin /usr/bin/bin
sudo chmod +x /usr/bin/bin
```

## Usage

There are two commands:

```shell
bin install <binary url> <name of the app> <icon url, optional>
bin remove <name of the app>
```

## Goal for the future

- [ ] Making **bin** more universal, and compatible with other Linux distros.

- [ ] Automatically detect the image inside the AppImage package

- [x] Creating a binary manager for the most well-known apps

## Contributing

If you want to contribute (thanks you!) you can get all the information you need [here](./CONTRIBUTE.md)

## License

This project is under GNU-GPL v3, more information [here](./LICENSE)
